#pragma once

#include "fat.h"

#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/spi.h>

#define SD_WAIT_FOR_ANSWER 1e5
#define SD_REPEAT_COMMANDS 50

/// Reset the SD Card
#define CMD0 0
/// Initialize SD Card
#define CMD1 1
/// Ask whether the card can operate with the voltage
#define CMD8 8
/// Set the block length
#define CMD16 16
/// Read single block
#define CMD17 17
/// Read multiple blocks
#define CMD18 18
/// Write single block
#define CMD24 24
/// Write multiple blocks
#define CMD25 25

class SdSpiIface
{
public:
	SdSpiIface(uint32_t spi);
	void init();
	
	bool open();
	void close();
	
	bool isOpen() const { return _opened; }
	
protected:
	void cs_low();
	void cs_high();
	
	uint8_t send(uint8_t data);
	uint8_t sendCmd(uint8_t cmd, uint32_t param, bool waitResponse = false);
	
public:
	bool readSector(uint32_t sector);
	bool writeSector(uint32_t sector);
	
	uint8_t buf[512];
	
	fat::Partition part;
	
private:
	uint32_t _spi;
	uint32_t _cs_port;
	uint16_t _cs_pin;
	bool _opened;
};
