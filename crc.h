#pragma once

#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

uint8_t crc7(uint8_t *chr, uint8_t len);
uint16_t crc16(uint16_t crc, uint8_t c);

#ifdef __cplusplus
}
#endif
