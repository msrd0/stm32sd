#include "crc.h"

uint8_t crc7(uint8_t* chr, uint8_t len)
{
	uint8_t crc = 0;
	uint8_t data;
	
	uint8_t i, j;
	
	for (i = 0; i < len; i++)
	{
		data = chr[i];
		for (j = 0; j < 8; j++)
		{
			crc <<= 1;
			if ((data & 0x80) ^ (crc & 0x80))
				crc ^= 0x09;
			data <<= 1;
		}
	}
	return (crc & 0x7f);
}

uint16_t crc16(uint16_t crc, uint8_t c)
{
	crc = (uint8_t)(crc >> 8)|(crc << 8);
	crc ^= c;
	crc ^= (crc & 0xFF) >> 4;
	crc ^= crc << 12;
	crc ^= (crc & 0xFF) << 5;
	return crc;
}
