#pragma once

#include <stdint.h>

class SdSpiIface;

#define FAT_MAX_NUM_FILES 50

namespace fat {

namespace datastructs {

#define PACKED __attribute((packed))

typedef struct {
	uint8_t first_byte;
	uint8_t start_chs[3];
	uint8_t partition_type;
	uint8_t end_chs[3];
	uint32_t start_sector;
	uint32_t length_sectors;
} PACKED PartitionTable;

typedef struct {
	uint8_t jmp[3];
	char oem[8];
	uint16_t sector_size;
	uint8_t sectors_per_cluster;
	uint16_t reserved_sectors;
	uint8_t number_of_fats;
	uint16_t root_dir_entries;
	uint16_t total_sectors_short; // if zero, later field is used
	uint8_t media_descriptor;
	uint16_t fat_size_sectors;
	uint16_t sectors_per_track;
	uint16_t number_of_heads;
	uint32_t hidden_sectors;
	uint32_t total_sectors_long;
	
	uint8_t drive_number;
	uint8_t current_head;
	uint8_t boot_signature;
	uint32_t volume_id;
	char volume_label[11];
	char fs_type[8];
	char boot_code[448];
	uint16_t boot_sector_signature;
} PACKED Fat16BootSector;

typedef struct {
	uint8_t filename[8];         // 0x00
	uint8_t ext[3];              // 0x08
	uint8_t attributes;          // 0x0B
	uint8_t reserved[10];        // 0x0C
	uint16_t modify_time;        // 0x17
	uint16_t modify_date;        // 0x19
	uint16_t starting_cluster;   // 0x1B
	uint32_t file_size;          // 0x1D
} PACKED Fat16Entry;

} // namespace datastructs

class Partition;
class Directory;
class File;

class File
{
public:
	File();
	File(Partition *part, const datastructs::Fat16Entry &entry);
	File(Partition *part, char name[8], char ext[3], uint32_t size);
	
	const char* name() const { return _name; }
	uint32_t start() const { return _start; }
	uint32_t size() const { return _size; }
	
	int8_t read();
	void write(char *buf, uint32_t len);
	
private:
	Partition *part = 0;
	
	char _name[13];
	uint32_t _start;
	uint32_t _size;
	
	uint32_t _in = 0;
	uint32_t _pos = 0;
	uint16_t _cluster;
	uint16_t _cluster_left;
};

class Directory
{
public:
	File files[FAT_MAX_NUM_FILES];
	uint8_t size = 0;
};

class Partition
{
	friend class File;
	
public:
	void init(SdSpiIface *iface, uint32_t sector, uint32_t len = 0);
	
	Directory root() const { return _root; }
	
private:
	uint16_t nextEmptyCluster(uint16_t start);
	
	uint32_t seek(uint32_t pos);
	uint32_t last_seek;
	
	SdSpiIface *sd = 0;
	
	uint32_t _sector;
	uint32_t _len;
	
	uint32_t of;
	uint32_t cluster_size;
	
	datastructs::Fat16BootSector bs;
	
	Directory _root;
};

}
