#include "crc.h"
#include "sd.h"

#include <stdio.h>

using namespace fat;

SdSpiIface::SdSpiIface(uint32_t spi)
	: _spi(spi)
	, _opened(false)
{
	switch (_spi) {
		case SPI1:
			_cs_port = GPIOA;
			_cs_pin  = GPIO_SPI1_NSS;
			break;
		case SPI2:
			_cs_port = GPIOB;
			_cs_pin  = GPIO_SPI2_NSS;
			break;
		default:
			printf("ERROR: SdSpiIface called with unknown spi interface\r\n");
	}
}

void SdSpiIface::init()
{
	switch (_spi) {
		case SPI1:
			// PA4=CS, PA5=SCK, PA6=MISO, PA7=MOSI
			gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_SPI1_SCK | GPIO_SPI1_MOSI);
			gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO_SPI1_MISO);
			break;
		case SPI2:
			// PB12=CS, PB13=SCK, PB14=MISO, PB15=MOSI
			gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_SPI2_SCK | GPIO_SPI2_MOSI);
			gpio_set_mode(GPIOB, GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO_SPI2_MISO);
			break;
	}
	gpio_set_mode(_cs_port, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, _cs_pin);
	
	spi_reset(_spi);
	spi_init_master(_spi, SPI_CR1_BAUDRATE_FPCLK_DIV_64, SPI_CR1_CPOL_CLK_TO_1_WHEN_IDLE,
					SPI_CR1_CPHA_CLK_TRANSITION_2, SPI_CR1_DFF_8BIT, SPI_CR1_MSBFIRST);
	spi_enable_software_slave_management(_spi);
	spi_set_nss_high(_spi);
	cs_high();
}

void SdSpiIface::cs_low()
{
	gpio_clear(_cs_port, _cs_pin);
}
void SdSpiIface::cs_high()
{
	gpio_set(_cs_port, _cs_pin);
}

bool SdSpiIface::open()
{
	uint32_t i;
	
	spi_enable(_spi);
	
	// wait a few clocks to get the sd card started
	for (i = 0; i < 20; i++) send(0xFF);
	cs_low();
	
	uint8_t res = sendCmd(0, 0, true);
	if (res != 1)
	{
		printf("SdSpiIface: SD Card answered %d to CMD0\r\n", res);
		cs_high();
		return false;
	}
	res = 0;
	i = 0;
	while (i++ < SD_REPEAT_COMMANDS)
	{
		res = sendCmd(1, 0, true);
		if (res == 0)
			break;
		else
			printf("SdSpiIface: SD Card answered %d to CMD1\r\n", res);
		for (uint8_t j = 0; j < 50; j++) __asm__("nop");
	}
	if (i >= SD_REPEAT_COMMANDS)
	{
		printf("SdSpiIface: Didn't get a valid answer to CMD1\r\n");
		cs_high();
		return false;
	}
	
	// set sector size
	res = sendCmd(16, 512, true);
	
	// fullspeed
	spi_set_baudrate_prescaler(_spi, SPI_CR1_BAUDRATE_FPCLK_DIV_2);
	
	// init the partition
	part.init(this, 0);
	
	_opened = true;
	return true;
}

bool SdSpiIface::readSector(uint32_t sector)
{
	uint32_t i;
	
	sendCmd(CMD17, sector<<9, false);
	// wait for 0
	for (i=0; i<SD_WAIT_FOR_ANSWER && send(0xFF)!=0; i++);
	if (i >= SD_WAIT_FOR_ANSWER)
		return false;
	// wait for data start
	for (i=0; i<SD_WAIT_FOR_ANSWER && send(0xFF)!=0xFE; i++);
	if (i >= SD_WAIT_FOR_ANSWER)
		return false;
	// read 512 bytes
	for (i=0; i < 512; i++)
		buf[i] = send(0xFF);
	return true;
}

bool SdSpiIface::writeSector(uint32_t sector)
{
	uint32_t i;
	uint16_t crc;
	
	crc = 0;
	for (i = 0; i < 512; i++)
		crc = crc16(crc, buf[i]);
	
	sendCmd(CMD24, sector<<9, false);
	// wait for 0
	for (i=0; i<SD_WAIT_FOR_ANSWER && send(0xFF)!=0; i++);
	if (i >= SD_WAIT_FOR_ANSWER)
		return false;
	// send the data
	send(0xFE);
	for (i=0; i < 512; i++)
		send(buf[i]);
	// send crc
	send((crc >> 8) & 0xFF);
	send(crc & 0xFF);
	// wait for data ack
	for (i=0; i<SD_WAIT_FOR_ANSWER && send(0xFF)!=0x02; i++);
	if (i >= SD_WAIT_FOR_ANSWER)
		return false;
	// wait for card to get idle again
	for (i=0; i<SD_WAIT_FOR_ANSWER && send(0xFF)!=0; i++);
	return true;
}

void SdSpiIface::close()
{
	cs_high();
	spi_disable(_spi);
	_opened = false;
}

uint8_t SdSpiIface::send(uint8_t data)
{
	spi_send(_spi, data);
	return spi_read(_spi);
}

uint8_t SdSpiIface::sendCmd(uint8_t cmd, uint32_t param, bool waitResponse)
{
	uint32_t i;
	
	uint8_t tosend[6];
	tosend[0] = cmd | 0x40;
	tosend[1] = param >> 24;
	tosend[2] = param >> 16;
	tosend[3] = param >> 8;
	tosend[4] = param;
	tosend[5] = (crc7(tosend, 5) << 1) | 1;
	for (i = 0; i < 6; i++)
		send(tosend[i]);
	
	if (!waitResponse)
		return 0xFF;
	uint8_t ret = 0xFF;
	i = 0;
	while ((ret == 0xFF) && (i++ < SD_WAIT_FOR_ANSWER))
		ret = send(0xFF);
	return ret;
}
