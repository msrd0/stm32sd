#include "fat.h"
#include "sd.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <algorithm>

using namespace fat;
using namespace fat::datastructs;

using std::min;

void Partition::init(SdSpiIface* iface, uint32_t sector, uint32_t len)
{
	sd = iface;
	_sector = sector;
	_len = len;
	assert(sd);
	
	sd->readSector(_sector);
	memcpy(&bs, sd->buf, sizeof(Fat16BootSector));
	
	of = _sector * 512 + (bs.reserved_sectors + bs.fat_size_sectors * bs.number_of_fats) * bs.sector_size;
	cluster_size = bs.sector_size * bs.sectors_per_cluster;
	
	Fat16Entry entry;
	for (int i = 0; i < bs.root_dir_entries; i++)
	{
		uint32_t in = seek(of + i*sizeof(Fat16Entry));
		memcpy(&entry, sd->buf+in, sizeof(Fat16Entry));
		
		if (!entry.filename[0]) break;
		if ((entry.attributes & 0x08) != 0 || (entry.attributes & 0x04) != 0) continue;
		
		if (entry.attributes == 0x0F) // LFN
		{
			continue;
		}
		
		_root.files[_root.size] = File(this, entry);
		_root.size++;
		if (_root.size == FAT_MAX_NUM_FILES)
			break;
	}
}

uint16_t Partition::nextEmptyCluster(uint16_t start)
{
	uint16_t cluster, cluster_val;
	uint32_t in;
	for (cluster = start; cluster > 0; cluster++)
	{
		in = seek(bs.reserved_sectors * bs.sector_size + cluster*2);
		memcpy((char*)(&cluster_val), sd->buf+in, 2);
		if (cluster_val == 0)
			break;
	}
	return cluster;
}

uint32_t Partition::seek(uint32_t pos)
{
	uint32_t tmp = pos / 512;
	if (tmp != last_seek)
	{
		sd->readSector(tmp);
		last_seek = tmp;
	}
	return (pos % 512);
}

File::File()
{
	_name[0] = 0;
}
File::File(Partition *part, const Fat16Entry& entry)
	: part(part)
	, _start(entry.starting_cluster)
	, _size(entry.file_size)
	, _cluster(_start)
	, _cluster_left(part->cluster_size)
{
	assert(part);
	
	memcpy(_name, entry.filename, 8);
	_name[8]  = '.';
	_name[9]  = entry.ext[0];
	_name[10] = entry.ext[1];
	_name[11] = entry.ext[2];
	_name[12] = 0;
}
File::File(fat::Partition* part, char name[8], char ext[3], uint32_t size)
	: part(part)
	, _start(part->nextEmptyCluster(3))
	, _size(size)
	, _cluster(_start)
	, _cluster_left(part->cluster_size)
{
	assert(part);
	
	memcpy(_name, name, 8);
	_name[8]  = '.';
	_name[9]  = ext[0];
	_name[10] = ext[1];
	_name[11] = ext[2];
	_name[12] = 0;
	
	uint16_t i;
	Fat16Entry entry;
	for (i = 0; i < part->bs.root_dir_entries; i++)
	{
		_in = part->seek(part->of + i*sizeof(entry));
		memcpy(&entry, part->sd->buf+_in, sizeof(entry));
		
		if (!entry.filename[0])
		{
			memcpy(entry.filename, name, 8);
			memcpy(entry.ext, ext, 3);
			entry.attributes = 0x20;
			memset(entry.reserved, 0, 10);
			entry.modify_time = 0;
			entry.modify_date = 0;
			entry.starting_cluster = _start;
			entry.file_size = _size;
			
			memcpy(part->sd->buf+_in, &entry, sizeof(entry));
			part->sd->writeSector(part->last_seek);
			break;
		}
	}
}


int8_t File::read()
{
	if (_pos >= _size)
		return EOF;
	if (_cluster_left <= 0)
	{
		_in = part->seek(part->bs.reserved_sectors * part->bs.sector_size + _cluster*2);
		memcpy(&_cluster, part->sd->buf+_in, 2);
		_cluster_left = part->cluster_size;
	}
	_in = part->seek(part->of + 0x8000 + part->cluster_size * (_cluster - 2) + (_pos % part->cluster_size));
	_pos++;
	_cluster_left--;
	return part->sd->buf[_in];
}

void File::write(char *buf, uint32_t len)
{
	uint16_t next_cluster;
	uint32_t i, toWrite;
	
	for (i = 0; i < len;)
	{
		if (_cluster_left <= 0)
		{
			next_cluster = part->nextEmptyCluster(_cluster+1);
			_in = part->seek(part->bs.reserved_sectors * part->bs.sector_size + _cluster*2);
			memcpy(part->sd->buf+_in, &next_cluster, 2);
			part->sd->writeSector(part->last_seek);
			_cluster = next_cluster;
			_cluster_left = part->cluster_size;
		}
		_in = part->seek(part->of + 0x8000 + part->cluster_size * (_cluster - 2) + (_pos % part->cluster_size));
		
		toWrite = min({ len-i, (uint32_t)_cluster_left, 512-_in });
		assert(toWrite > 0);
		
		memcpy(part->sd->buf+_in, buf+i, toWrite);
		part->sd->writeSector(part->last_seek);
		
		i += toWrite;
		_cluster_left -= toWrite;
		_pos += toWrite;
	}
	
	next_cluster = 0xFFFF;
	_in = part->seek(part->bs.reserved_sectors * part->bs.sector_size + _cluster*2);
	memcpy(part->sd->buf+_in, &next_cluster, 2);
	part->sd->writeSector(part->last_seek);
}
